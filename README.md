#### Installation

Make sure you have `node`, `npm`, `gulp` and `bower` installed, if you don't run `npm install gulp -g` & `npm install bower -g`

* `npm install`
* `bower install`

To run the project locally: `gulp serve`.

Project details [https://bitbucket.org/AlfaProject/notes-frontend/wiki/Home](https://bitbucket.org/AlfaProject/notes-frontend/wiki/Home)